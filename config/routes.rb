Rails.application.routes.draw do
  devise_for :users
  devise_for :utilisateurs
  namespace 'api' do
    namespace 'v1' do
      root to: "utilisateurs#index"
      resources :roles
      resources :utilisateurs
      resources :contacts
      resource :sessions, only: [:create, :destroy]
    end
  end
end
