class Utilisateur < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :role_id, presence: true
  validates :statut_id, presence: true
  validates :actif, presence: true
  validates :nom, presence: true
  validates :prenom, presence: true
  validates :mail, presence: true
  validates :telephone, presence: true
  validates :pass, presence: true
end
