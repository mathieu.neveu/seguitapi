module Api
  module V1
    class UtilisateursController < ApplicationController
      def index
        utilisateurs = Utilisateur.order('created_at DESC');
        render json: {status:'success',utilisateur:utilisateurs},status: :ok
      end

      def show
        utilisateur = Utilisateur.find(params[:id])
        render json: {status:'success',utilisateur:utilisateur},status: :ok
      end

      def create
        nouvUtilisateur = Utilisateur.new(newUtilisateur_params)
        if nouvUtilisateur.save
          render json: {status:'success',data:nouvUtilisateur}
        else
          render json: {status:'error',data:nouvUtilisateur},status: :unprocessable_entity
        end
      end
      def newUtilisateur_params
        params.permit(:role_id,:statut_id,:actif,:nom,:prenom,:mail,:telephone,:pass)
      end

      def destroy
        utilisateurASupp = Utilisateur.find(params[:id])
        if utilisateurASupp.delete
          render json: {status:'success',data:utilisateurASupp}
        else
          render json: {status:'echec',data:utilisateurASupp}
        end
      end

      def update
        utilisateurAModifier = Utilisateur.find(params[:id])
        if utilisateurAModifier.update_attributes(utilisateurAModifier_params)
          render json: {status:'reussite',data:utilisateurAModifier}
        else
          render json: {status:'echec',data:utilisateurAModifier}
        end
      end
      def utilisateurAModifier_params
        params.permit(:role_id,:statut_id,:actif,:nom,:prenom,:mail,:telephone,:pass)
      end
    end
  end
end