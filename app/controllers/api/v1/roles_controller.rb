module Api
  module V1
    class RolesController < ApplicationController
      def index
        roles = Role.order('created_at DESC')
        render json: {statut: 'succès',data:roles},status: :ok
      end
    end
  end
end