class CreateStatuts < ActiveRecord::Migration[6.0]
  def change
    create_table :statuts do |t|
      t.string :nom

      t.timestamps
    end
  end
end
