class CreateUtilisateurs < ActiveRecord::Migration[6.0]
  def change
    create_table :utilisateurs do |t|
      t.bigint :role_id
      t.bigint :statut_id
      t.boolean :actif
      t.string :nom
      t.string :prenom
      t.string :mail
      t.string :telephone
      t.string :pass

      t.timestamps
    end
  end
end
